package be.kdg.java2.demojpa;

import be.kdg.java2.demojpa.domain.Book;
import be.kdg.java2.demojpa.domain.Genre;
import jakarta.persistence.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

@Component
public class DemoRunner implements CommandLineRunner {
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Override
	public void run(String... args) throws Exception {
		Book book = new Book("Hitchhiker's Guide", 120,
			LocalDate.now(), LocalTime.NOON, Genre.FANTASY);
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			em.persist(book);
			System.out.println(book);
			em.getTransaction().commit();
		}
		//Let's insert 100 books into the database!
		try (EntityManager entityManager = entityManagerFactory.createEntityManager()) {
			entityManager.getTransaction().begin();
			Stream.generate(Book::randomBook).limit(100).forEach(
				entityManager::persist
			);
			entityManager.getTransaction().commit();
		}
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			book = em.find(Book.class, 20);//we find Book with ID 20
			System.out.println("Found book: " + book);
			System.out.println("Changing the number of pages:");
			book.setPages(1);
			em.getTransaction().commit();
		}
		//Query using JPA Query Language:
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			List books = em.createQuery("select b from Book b where b.title like 'title1%'")
				.getResultList(); // query returning a non generic list (objects)
			books.forEach(System.out::println);
		}

		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			TypedQuery<Book> query = em.createQuery("select b from Book b where b.title = :title",
				Book.class);
			query.setParameter("title", "Hitchhiker's Guide");
			List<Book> books = query.getResultList();
			books.forEach(System.out::println);
		}
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			book = em.find(Book.class, 23);
			//book is still managed by the EnityManager!
			book.setGenre(Genre.FANTASY);
			em.getTransaction().commit();
		}


		// update a detached book
		book.setTitle("Updated title...");
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			em.merge(book);
			em.getTransaction().commit();
		}

		//Let's delete a book:
		try (EntityManager em = entityManagerFactory.createEntityManager()) {
			em.getTransaction().begin();
			book = em.find(Book.class, 1);
			em.remove(book);
			em.getTransaction().commit();
		}
	}
}
