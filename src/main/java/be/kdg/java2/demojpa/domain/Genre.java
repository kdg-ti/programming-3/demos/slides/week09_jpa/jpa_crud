package be.kdg.java2.demojpa.domain;

public enum Genre {
    FANTASY, THRILLER, COMEDY
}
