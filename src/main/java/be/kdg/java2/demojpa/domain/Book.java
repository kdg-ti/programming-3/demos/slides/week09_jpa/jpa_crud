package be.kdg.java2.demojpa.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "book_title", nullable = false, length = 50)
    private String title;
    private int pages;
    private LocalDate datePublished;
    private LocalTime bestReadingTime;
    @Enumerated(EnumType.STRING)
    private Genre genre;

    //JPA needs this...
    public Book() {
    }

    public Book(String title, int pages, LocalDate datePublished, LocalTime bestReadingTime, Genre genre) {
        this.title = title;
        this.pages = pages;
        this.datePublished = datePublished;
        this.bestReadingTime = bestReadingTime;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public LocalTime getBestReadingTime() {
        return bestReadingTime;
    }

    public void setBestReadingTime(LocalTime bestReadingTime) {
        this.bestReadingTime = bestReadingTime;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", datePublished=" + datePublished +
                ", bestReadingTime=" + bestReadingTime +
                ", genre=" + genre +
                '}';
    }

    //Factory method to create random books:
    public static Book randomBook() {
        Random random = new Random();
        return new Book("title" + random.nextInt(1000),
                random.nextInt(1000),
                LocalDate.of(1900 + random.nextInt(100), random.nextInt(12) + 1, random.nextInt(28) + 1),
                LocalTime.of(random.nextInt(24), random.nextInt(60)),
                Genre.values()[random.nextInt(Genre.values().length)]);
    }

}
